
# Conference management system #

Master rad: **Primena reaktivnog programiranja i Spring okvira u razvoju Java aplikacija**

Fakultet organizacionih nauka, Univerzitet u Beogradu

### Opis ###
Conference management system je sistem namenjen za upravljenje korisnickim prijavama na stručnim konferencijama.
Pored glavnog usera SUPERADMIN, koji kreira konferencije, korisnici imaju tri vrste uloga:
- admin konferencije - CONFERENCE_ADMIN
- strucni clan - COMMITTEE_MEMBER
- ucesnik konferencije (korisnik) - USER

Za potrebe master rada, fokus je na strucnom clanu i ucesniku konferencije, kao korisnicima koji imaju najvise
slucajeva koriscenja. Admin konferencije bi imao sve privilegije kao i strucni clan, sa dodatkom kreiranje
raznih specificnih podataka za konferenciju.

Korisnik se prijavuje/registruje na sistem, nakon cega ima mogucnost da bira konferenciju koju zeli da otvori
ili pristupa direktnim linkom na stranicu konferencije. Ukoliko korisnik prvi put zeli da poseti stranicu konferencije,
morace da podnese zahtev za registracijom na aplikaciju konferencije, koju odobrava/odbija strucni clan ili admin.
Kada je ucesniku odobrena registracija, on moze podneti prijavu za konferenciju. Prijava se sastoji od strucnog rada,
u vidu dokumenta, kao i pratecih podataka uz prijavu (opis, kategorija,..).
Nakon kreirane prijave, strucni clan uzima aplikaciju na pregledanje. Tokom pregledanja, strucni clan i ucesnik
mogu razmenjivati komentare na stranici aplikacije. A nakon pregledannja strucni clan prihvata ili odbija aplikaciju.
Ukoliko korisnik 2 odobravanja (2 strucna clana su odobrila aplikaciju), on moze potvrditi svoju prijavu,
te postaje ucesnik predstojece konferencije.

### Arhitektura ###
Aplikacija koristi "headless" arhitekturu. To znaci da postoje dva dela aplikacije, Frontend i Backend deo.

BE deo aplikacije implementiran tako da postuje principe reaktivniog programiranje, tako da je ceo sistem reaktivan
ukljucujuci i pristup bazi. Napsan je pomocu Spring frameworka i projekta Reactor.
Kako bi i pristup bazi, kao blokirajuci proces, bio asinhron i reaktivan, koriscen je r2dbc-mysql driver.
https://bitbucket.org/bskoric/conference-management-system/src/master/

Frontend deo aplikacije implementiran je u Reactjs.

Za potrebe testiranja, svi postojeci korisnici, koji su vec u bazi podataka, koriste istu lozinku 'Asd12345'.


# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
