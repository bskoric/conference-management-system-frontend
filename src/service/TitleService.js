import {TITLES_BASE} from '../const/Const';
import DataService from './DataService';

class TitleService {

    getAll() {
        return DataService.getWithAuth(TITLES_BASE);
    }
}

export default new TitleService();