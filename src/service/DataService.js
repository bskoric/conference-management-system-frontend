import Axios from 'axios/index';
import AuthService from './AuthService';

class DataService {

    get(url) {
        return Axios.get(url);
    }

    getWithAuth(url) {
        AuthService.setupAxiosInterceptorsRequest();
        AuthService.setupAxiosInterceptorsResponse();
        return this.get(url);
    }

    getFile(url) {
        AuthService.setupAxiosInterceptorsRequest();
        return Axios.get(url, {responseType: 'arraybuffer'});
    }

    post(url, data) {
        return Axios({
            method:  'post',
            url:     url,
            headers: {
                'Accept':       'application/json',
                'Content-Type': 'application/json'
            },
            data:    data
        });
    }

    postWithAuth(url, data) {
        AuthService.setupAxiosInterceptorsRequest();
        AuthService.setupAxiosInterceptorsResponse();
        return this.post(url, data)
    }

    delete(url, data) {
        AuthService.setupAxiosInterceptorsRequest();
        return Axios({
            method:  'delete',
            url:     url,
            headers: {
                'Accept':       'application/json',
                'Content-Type': 'application/json'
            },
            data:    data
        });
    }

    add(url, item) {
        return Axios({
            method:  'post',
            url:     url,
            headers: {
                'Accept': 'application/json'
            },
            data:    item
        });
    }

    update(url, item) {
        return Axios({
            method:  'put',
            url:     url,
            headers: {
                'Accept':       'application/json',
                'Content-Type': 'application/json'
            },
            data:    item
        });
    }
}

export default new DataService();