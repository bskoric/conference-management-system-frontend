import DataService from './DataService';
import {REVIEWS_BASE} from '../const/Const';

class ReviewService {

    getReviews(conferenceID, userId) {
        return DataService.getWithAuth(REVIEWS_BASE + '/conferences/' + conferenceID + '/users/' + userId)
    }

    getReviewsByApplication(applicationId) {
        return DataService.getWithAuth(REVIEWS_BASE + '/applications/' + applicationId);
    }

    getReviewsByCommitteeMemberAndApplyStatus(committeeMemberId, conferenceId, status) {
        return DataService.getWithAuth(
                REVIEWS_BASE + '/conferences/' + conferenceId + '/committee-member/' + committeeMemberId + '/apply-status/' + status)
    }

    getReview(reviewId) {
        return DataService.getWithAuth(REVIEWS_BASE + '/' + reviewId)
    }

    changeStatus(reviewId, status) {
        return DataService.postWithAuth(REVIEWS_BASE + '/' + reviewId + '/status?type=' + status);
    }

    createReview(data) {
        return DataService.postWithAuth(REVIEWS_BASE, data);
    }
}

export default new ReviewService();