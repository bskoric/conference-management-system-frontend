import {DOCUMENT_BASE, DOCUMENT_CATEGORIES} from '../const/Const';
import DataService from './DataService';

class DocumentService {

    getDocument(id) {
        return DataService.getWithAuth(DOCUMENT_BASE + '/' + id)
    }

    getFile(id) {
        return DataService.getFile(DOCUMENT_BASE + '/' + id + '/file')
    }

    getCategories() {
        return DataService.getWithAuth(DOCUMENT_CATEGORIES);
    }

    getCategory(categoryId) {
        return DataService.getWithAuth(DOCUMENT_CATEGORIES + "/" + categoryId);
    }
}

export default new DocumentService();