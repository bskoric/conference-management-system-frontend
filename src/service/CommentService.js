import DataService from './DataService';
import {APPLICATION_BASE} from '../const/Const';

class CommentService {

    getCommentsForApplication(applicationId) {
        return DataService.getWithAuth(APPLICATION_BASE + '/' + applicationId + '/comments');
    }

    addComment(applicationId, comment) {
        return DataService.postWithAuth(APPLICATION_BASE + '/' + applicationId + '/comments', comment);
    }

    deleteComment(commentId, userID) {
        return DataService.delete(APPLICATION_BASE + '/comments/' + commentId + '/users/' + userID);
    }
}

export default new CommentService();