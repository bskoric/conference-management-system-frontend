import Axios from 'axios/index';
import DataService from './DataService';

export const USERNAME_SESSION_ATTRIBUTE_NAME = 'AuthUsername';
export const USER_SESSION_ATTRIBUTE_NAME = 'AuthUser';
export const USER_SESSION_TOKEN = 'AuthToken';

class AuthService {

    login(username, password, url) {
        return DataService.post(url, {
            username: username,
            password: password
        })
    }

    register(data, url) {
        return DataService.post(url, data);
    }

    logout() {
        sessionStorage.removeItem(USER_SESSION_ATTRIBUTE_NAME);
        sessionStorage.removeItem(USERNAME_SESSION_ATTRIBUTE_NAME);
        sessionStorage.removeItem(USER_SESSION_TOKEN);
        window.location.reload();
    }

    createBasicAuthToken() {
        return 'Bearer ' + this.getLoggedInToken();
    }

    setupAxiosInterceptorsRequest() {
        const token = this.createBasicAuthToken();
        Axios.interceptors.request.use(
                (config) => {
                    if (this.isUserLoggedIn()) {
                        config.headers.authorization = token
                    }
                    return config
                }
        )
    }

    setupAxiosInterceptorsResponse() {
        Axios.interceptors.response.use((response) => {
            return response;
        }, (error) => {
            console.log(error.response)
            if (error.response && error.response.status === 401) {
                sessionStorage.removeItem(USERNAME_SESSION_ATTRIBUTE_NAME);
                window.location = '/'
            } else {
                return Promise.reject(error);
            }
        });
    }

    putInSessionStorage(username, user, token) {
        sessionStorage.setItem(USERNAME_SESSION_ATTRIBUTE_NAME, username);
        sessionStorage.setItem(USER_SESSION_ATTRIBUTE_NAME, user);
        sessionStorage.setItem(USER_SESSION_TOKEN, token);
    }

    isUserLoggedIn() {
        const username = sessionStorage.getItem(USERNAME_SESSION_ATTRIBUTE_NAME);
        const user = sessionStorage.getItem(USER_SESSION_ATTRIBUTE_NAME);
        const token = sessionStorage.getItem(USER_SESSION_TOKEN);
        return ((user != null && user.length !== 0) && (username != null && username.length !== 0) && (token != null && token.length
                !== 0));

    }

    getLoggedInUser() {
        const user = sessionStorage.getItem(USER_SESSION_ATTRIBUTE_NAME);
        if (user === null) {
            return null;
        }
        return JSON.parse(user);
    }

    getLoggedInUsername() {
        const username = sessionStorage.getItem(USERNAME_SESSION_ATTRIBUTE_NAME);
        if (username === null) {
            return '';
        }
        return username
    }

    getLoggedInToken() {
        const token = sessionStorage.getItem(USER_SESSION_TOKEN);
        if (!token) {
            return '';
        }
        return token
    }
}

export default new AuthService()
