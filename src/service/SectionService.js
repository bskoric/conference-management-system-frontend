import DataService from './DataService';
import {SECTIONS_BASE} from '../const/Const';

class SectionService {

    getSection(id) {
        return DataService.getWithAuth(SECTIONS_BASE + '/' + id);
    }

    getConferenceSectionsForCategory(categoryId) {
        return DataService.getWithAuth(SECTIONS_BASE + '/categories/' + categoryId)
    }

    getConferenceSectionsForCommitteeMember(committeeMemberId) {
        return DataService.getWithAuth(SECTIONS_BASE + '/committee-member/' + committeeMemberId)
    }
}

export default new SectionService();