import DataService from './DataService';
import {USERS_BASE} from '../const/Const';

class UserService {

    getUser(id) {
        return DataService.getWithAuth(USERS_BASE + '/' + id);
    }

}

export default new UserService();