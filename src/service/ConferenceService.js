import DataService from './DataService';
import {APPLICATION_BASE, CONFERENCES_APPLY, CONFERENCES_BASE} from '../const/Const';

class ConferenceService {

    getAllConferences() {
        return DataService.get(CONFERENCES_BASE);
    }

    getConference(id) {
        return DataService.getWithAuth(CONFERENCES_BASE + '/' + id);
    }

    getConferenceCategory(id) {
        return DataService.getWithAuth(CONFERENCES_BASE + '/' + id);
    }

    isMemberOfConference(conferenceId, userId) {
        return DataService.getWithAuth(CONFERENCES_BASE + '/' + conferenceId + '/users/' + userId + '/is-member');
    }

    registerToConferenceApp(conferenceId, userId) {
        return DataService.postWithAuth(CONFERENCES_BASE + '/' + conferenceId + '/users/' + userId + '/register');
    }

    applyToConference(document) {
        return DataService.postWithAuth(CONFERENCES_APPLY, document);
    }

    getApplications(conferenceId, userId) {
        return DataService.getWithAuth(CONFERENCES_APPLY + '/conferences/' + conferenceId + '/users/' + userId)
    }

    getApplicationsForReview(conferenceId, committeeMemberId) {
        return DataService.getWithAuth(APPLICATION_BASE + '/conferences/' + conferenceId + '/committee-member/' + committeeMemberId)
    }

    getApplication(applicationId) {
        return DataService.getWithAuth(APPLICATION_BASE + '/' + applicationId);
    }

    getApplicationByIdAndConference(applicationId, conferenceId) {
        return DataService.getWithAuth(APPLICATION_BASE + '/' + applicationId + '/conferences/' + conferenceId);
    }

    finishApplication(applicationId) {
        return DataService.postWithAuth(APPLICATION_BASE + '/' + applicationId + '/finish');
    }

    closeApplication(applicationId) {
        return DataService.postWithAuth(APPLICATION_BASE + '/' + applicationId + '/close');
    }

    deleteApplication(applicationId) {
        return DataService.postWithAuth(APPLICATION_BASE + '/' + applicationId + '/delete');
    }
}

export default new ConferenceService();