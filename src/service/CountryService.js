import {COUNTRIES_BASE} from '../const/Const';
import DataService from './DataService';

class CountryService {

    getAll() {
        return DataService.getWithAuth(COUNTRIES_BASE);
    }
}

export default new CountryService();