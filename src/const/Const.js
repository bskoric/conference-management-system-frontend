/* API */
export const BASE_URL = 'http://localhost:8080/api';
export const LOGIN_URL = BASE_URL + '/users/auth/token';
export const REGISTER_URL = BASE_URL + '/users/auth/register';

export const CONFERENCES_BASE = BASE_URL + '/conferences'
export const CONFERENCE_CATEGORY = BASE_URL + '/conferences/categories'

export const APPLICATION_BASE = BASE_URL + '/applications'
export const CONFERENCES_APPLY = APPLICATION_BASE;

export const TITLES_BASE = BASE_URL + '/users/titles'
export const COUNTRIES_BASE = BASE_URL + '/countries'

export const DOCUMENT_BASE = BASE_URL + '/documents'
export const DOCUMENT_CATEGORIES = DOCUMENT_BASE + '/categories'

export const SECTIONS_BASE = BASE_URL + '/sections'

export const REVIEWS_BASE = BASE_URL + '/reviews'

export const USERS_BASE = BASE_URL + '/users'

/* ROLE AND STATUS */
export const ROLE_USER = 'USER'
export const ROLE_MEMBER = 'COMMITTEE_MEMBER'

export const USER_STATUS_PENDING = 'PENDING_APP_REGISTRATION';

export const APPLY_STATUS_DECLINED = 'DECLINED';
export const APPLY_STATUS_IN_REVIEW = 'IN_REVIEW';
export const APPLY_STATUS_APPLIED = 'APPLIED';
export const APPLY_STATUS_CLOSED = 'CLOSED';

export const REVIEW_STATUS_DECLINED = 'DECLINED';
export const REVIEW_STATUS_APPROVED = 'APPROVED';
export const REVIEW_STATUS_IN_PROGRESS = 'IN_PROGRESS';