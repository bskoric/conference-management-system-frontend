import {BrowserRouter as Router, Switch} from 'react-router-dom';
import AuthenticatedRoute from './components/auth/AuthenticatedRoute';
import Layout from './components/layout/Layout';
import Login from './components/auth/Login';
import MainHomepage from './components/pages/MainHomepage';
import AccessDenied from './components/pages/AccessDenied';
import React from 'react';
import Register from './components/auth/Register';

function App() {
    return (
            <div className="App">
                <Router>
                    <Switch>
                        {/*<AuthenticatedRoute path='/login/:id(\d+)' login={true} exact={true} component={Login}/>*/}
                        <AuthenticatedRoute path="/login" login={true} exact={true} component={Login}/>
                        <AuthenticatedRoute path="/register" login={true} exact={true} component={Register}/>
                        <AuthenticatedRoute path="/" exact={true} component={MainHomepage}/>
                        <AuthenticatedRoute path="/denied" exact={true} component={AccessDenied}/>
                        <AuthenticatedRoute path="/**" exact={true} component={Layout}/>
                    </Switch>
                </Router>
            </div>
    );
}

export default App;
