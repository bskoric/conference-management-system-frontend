import React from 'react';
import Container from '@material-ui/core/Container';
import {Button, Typography} from '@material-ui/core';
import logo from '../../assets/images/logo/CONFMS.png';

class AccessDenied extends React.Component {

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.history.push('/')
    }

    render() {
        return (
                <Container maxWidth="md">
                    <div className="responsive-image">
                        <img src={logo} alt={'Logo'} className="responsive-image__image"/>
                    </div>
                    <div style={{marginTop: '20px'}}>
                        <Typography variant="h4" component="h4" gutterBottom align={'center'} style={{color: '#463f57'}}>
                            Access denied ! You do not have permission!
                        </Typography>
                        <Typography variant="h4" component="h4" gutterBottom align={'center'} style={{color: '#463f57'}}>
                            <Button variant="outlined" size="medium" onClick={this.handleSubmit}>
                                Go to main page
                            </Button>
                        </Typography>
                    </div>
                </Container>
        );
    }
}

export default AccessDenied;