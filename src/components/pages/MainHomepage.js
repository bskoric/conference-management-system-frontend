import React, {Component} from 'react';
import ConferenceService from '../../service/ConferenceService';
import {makeStyles} from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import './../../assets/css/homepage.css';
import {Button, Container, Divider, Icon, Typography} from '@material-ui/core';
import logo from './../../assets/images/logo/CONFMS.png';
import AuthService from '../../service/AuthService';

class MainHomepage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            conferences:          [],
            selectedConferenceId: 0
        }
        this.getAllConferences = this.getAllConferences.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.getAllConferences();
    }

    getAllConferences() {
        ConferenceService.getAllConferences().then(res => {
            this.setState({
                        conferences: res.data
                    }
            );
        })
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.history.push('/conference/' + this.state.selectedConferenceId)
    }

    handleChange(event) {
        const value = event.target.value;
        this.setState({selectedConferenceId: value});
    }

    render() {
        const classes = makeStyles((theme) => ({
            formControl: {
                margin:   theme.spacing(1),
                minWidth: 120
            },
            selectEmpty: {
                marginTop: theme.spacing(2)
            }
        }));
        return (
                <Container maxWidth="md">
                    <Button variant="text" size="small" onClick={AuthService.logout}>
                        Log out
                    </Button>
                    <div className={'headlineLogo'}>
                        <div className="responsive-image">
                            <img src={logo} alt={'Logo'} className="responsive-image__image"/>
                        </div>
                        <Typography variant="h4" component="h4" gutterBottom align={'center'} style={{color: '#463f57'}}>
                            Conference management system
                        </Typography>
                        <Typography variant="h5" component="h5" gutterBottom align={'center'} style={{color: '#463f57'}}>
                            Welcome {AuthService.getLoggedInUsername()}
                        </Typography>
                        <Divider/>
                    </div>
                    <div className={'conferencesSelect'}>
                        <FormControl variant="outlined" className={classes.formControl}>
                            <InputLabel id="demo-simple-select-outlined-label">Conferences</InputLabel>
                            <Select labelId="demo-simple-select-outlined-label" id="demo-simple-select-outlined"
                                    onChange={this.handleChange}>
                                {this.state.conferences.map(conf => <MenuItem key={conf.id} value={conf.id}>{conf.name}</MenuItem>)}
                            </Select>
                            <Button
                                    type="submit"
                                    variant="contained"
                                    color="primary"
                                    endIcon={<Icon>send</Icon>}
                                    style={{marginTop: 20}}
                                    className={'button'}
                                    onClick={this.handleSubmit}
                            >
                                Go
                            </Button>
                        </FormControl>
                    </div>
                </Container>
        );
    }
}

export default MainHomepage;