import React, {Component} from 'react';
import ConferenceService from '../../service/ConferenceService';
import './../../assets/css/homepage.css';
import {Redirect} from 'react-router-dom';
import UserHomepage from './user/UserHomepage';
import AuthService from '../../service/AuthService';
import {ROLE_MEMBER, ROLE_USER, USER_STATUS_PENDING} from '../../const/Const';
import CommitteeMemberHomepage from './committee-member/CommitteeMemberHomepage';
import ConferenceSignUp from './ConferenceSignUp';

class ConferenceHomepage extends Component {

    emptyConference = {
        id:       0,
        name:     '',
        date:     '',
        town:     '',
        country:  {
            id:      0,
            name:    '',
            isoCode: ''
        },
        category: {
            id:          0,
            name:        '',
            description: ''
        },
        closed:   false
    };

    constructor(props) {
        super(props);
        this.state = {
            conference:                  this.emptyConference,
            userStatus:                  ''
        }
        this.getConference = this.getConference.bind(this);
    }

    componentDidMount() {
        const {match: {params}} = this.props;
        this.getConference(params.id);

        ConferenceService.isMemberOfConference(params.id, AuthService.getLoggedInUser().id).then(res => {
            if (res.data.userRegisterToConferenceApp) {
                this.setState({
                    userRegisterToConferenceApp: true,
                    userStatus:                  res.data.status
                })
            } else {
                this.setState({
                    userRegisterToConferenceApp: false,
                    userStatus:                  res.data.status
                })
            }
        });
    }

    getConference(conferenceId) {
        ConferenceService.getConference(conferenceId).then(res => {
            this.setState({
                        conference: res.data
                    }
            );
        })
    }

    render() {
        const role = AuthService.getLoggedInUser().role.name;

        if (this.state.userRegisterToConferenceApp === false && role === ROLE_MEMBER) {
            return (<Redirect to={'/denied'}/>);
        }

        if (this.state.userRegisterToConferenceApp === false && role === ROLE_USER) {
            return (<ConferenceSignUp conference={this.state.conference} pendingRegistration={false}/>);
        }

        if (this.state.userRegisterToConferenceApp === true && role === ROLE_USER && this.state.userStatus === USER_STATUS_PENDING) {
            return (<ConferenceSignUp conference={this.state.conference} pendingRegistration/>);
        }

        return (
                <React.Fragment>
                    {role === ROLE_USER && <UserHomepage conference={this.state.conference}/>}
                    {role === ROLE_MEMBER && <CommitteeMemberHomepage conference={this.state.conference}/>}
                </React.Fragment>
        );
    }
}

export default ConferenceHomepage;