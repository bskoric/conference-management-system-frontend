import React, {Component} from 'react';
import {Container, Typography} from '@material-ui/core';
import {Redirect, withRouter} from 'react-router-dom';
import MainFeaturedPost from '../../layout/MainFeaturedPost';
import ConferenceService from '../../../service/ConferenceService';
import {emptyConference} from '../../conference/ConferenceHelper';
import AuthService from '../../../service/AuthService';
import Grid from '@material-ui/core/Grid';
import ReviewCard from '../../review/ReviewCard';
import ReactPaginate from 'react-paginate';
import ReviewService from '../../../service/ReviewService';
import TransitionAlerts from '../../layout/TransitionAlerts';
import MemberHeader from '../../layout/header/MemberHeader';

class FindApplicationPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            conference:    emptyConference,
            applications:  [],
            reviewToRedirect: {},
            offset:        0,
            perPage:       4,
            currentPage:   0,
            alertMessage:  '',
            errorSeverity: '',
            showAlert:     false
        }

        this.getConference = this.getConference.bind(this);
        this.getApplications = this.getApplications.bind(this);
        this.handleCreateReview = this.handleCreateReview.bind(this);
    }

    componentDidMount() {
        const {match: {params}} = this.props;
        const conferenceId = params.id;

        this.getConference(conferenceId);
        this.getApplications(conferenceId, AuthService.getLoggedInUser().id);
    }

    getConference(conferenceId) {
        ConferenceService.getConference(conferenceId).then(res => {
            this.setState({
                        conference: res.data
                    }
            );
        })
    }

    getApplications(conferenceId, committeeId) {
        ConferenceService.getApplicationsForReview(conferenceId, committeeId).then(res => {
            const data = res.data.body
            const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage);
            this.setState({
                        applications: slice,
                        pageCount:    Math.ceil(data.length / this.state.perPage)
                    }
            );
        })
    }

    handleCreateReview(application, committeeMember) {

        const reviewRequestObject = {
            committeeMember: committeeMember,
            application:     application
        }

        ReviewService.createReview(reviewRequestObject).then(res => {
            this.setState({
                        errorSeverity: 'success',
                        alertMessage:  'Your are added to reviewers !',
                        showAlert:     true,
                        reviewToRedirect: res.data.body
                    }
            );

            setTimeout(() => {
                this.setState({
                    redirect: true
                })
            }, 2000);

        }).catch((error) => {
            this.setState({
                errorSeverity: 'error',
                alertMessage:  error.response.data.errorMessage,
                showAlert:     true
            });
        })
    }

    handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * this.state.perPage;

        this.setState({
            currentPage: selectedPage,
            offset:      offset
        }, () => {
            const {match: {params}} = this.props;
            this.getApplications(params.id, AuthService.getLoggedInUser().id);
        });

    };

    onCloseAlert = () => {
        this.setState({
            showAlert: false
        });
    }

    render() {
        const {conference} = this.state;

        if (this.state.redirect) {
            return (<Redirect to={'/conference/' + conference.id  + '/reviews/' + this.state.reviewToRedirect.id}/>);
        }

        return (
                <
                        React.Fragment>
                    < MemberHeader
                            conferenceId={conference.id}
                    />

                    <Container maxWidth="xl" style={{marginTop: '25px'}}>
                        <MainFeaturedPost title={conference.name}
                                          description={'Applications'}
                                          height="200px"
                        />
                    </Container>

                    <Container maxWidth={'md'}>
                        <div className={'aboutConfSection'}>
                            <Typography variant="h5" component="h5" align="center" gutterBottom>
                                Find applications to review
                            </Typography>
                            <Typography variant="body1" align={'center'} gutterBottom>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Morbi porta tincidunt pulvinar. Sed mattis dapibus mi, in cursus est sollicitudin eu.
                            </Typography>
                        </div>

                    </Container>

                    <Container maxWidth="lg" style={{padding: '15px', marginTop: '30px'}}>

                        {this.state.showAlert && <TransitionAlerts alertMessage={this.state.alertMessage}
                                                                   severity={this.state.errorSeverity}
                                                                   close={this.onCloseAlert}/>}

                        <Typography color="textPrimary" variant="h4" align={'center'}>
                            Applications
                        </Typography>
                        <Grid container spacing={2} style={{marginTop: '30px'}}>
                            {this.state.applications.map((app) => (
                                    <Grid item xs={12} sm={6} key={app.id}>
                                        <ReviewCard button={'Start review'}
                                                    application={app}
                                                    handleButton={() => this.handleCreateReview(app, AuthService.getLoggedInUser())}
                                        />
                                    </Grid>
                            ))}
                        </Grid>
                        <ReactPaginate
                                previousLabel={'prev'}
                                nextLabel={'next'}
                                breakLabel={'...'}
                                breakClassName={'break-me'}
                                pageCount={this.state.pageCount}
                                marginPagesDisplayed={2}
                                pageRangeDisplayed={5}
                                onPageChange={this.handlePageClick}
                                containerClassName={'pagination'}
                                subContainerClassName={'pages pagination'}
                                activeClassName={'active'}/>
                    </Container>

                </React.Fragment>
        );
    }

}

export default withRouter(FindApplicationPage);