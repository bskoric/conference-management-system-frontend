import React, {Component} from 'react';
import './../../../assets/css/homepage.css'
import '../../../assets/css/member-homepage.css'
import {Container, Icon, Typography} from '@material-ui/core';
import MemberHeader from '../../layout/header/MemberHeader';
import MainFeaturedPost from '../../layout/MainFeaturedPost';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import {withRouter} from 'react-router-dom';
import AuthService from '../../../service/AuthService';
import SectionService from '../../../service/SectionService';

class CommitteeMemberHomepage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            conference:     this.props.conference,
            memberSections: [],
            redirect:       false
        }
    }

    componentDidMount() {
        this.getSections(AuthService.getLoggedInUser().id);
    }

    getSections = (committeeMemberId) => {
        SectionService.getConferenceSectionsForCommitteeMember(committeeMemberId).then(res => {
            this.setState({
                memberSections: res.data
            })
        })
    }

    handleFindAppButton = (event) => {
        event.preventDefault();
        this.props.history.push('/conference/' + this.props.conference.id + '/applications')
    }

    handleSeeReviewsButton = (event) => {
        event.preventDefault();
        this.props.history.push('/conference/' + this.props.conference.id + '/reviews')
    }

    render() {
        const {conference} = this.props;
        const user = AuthService.getLoggedInUser();
        const user_fullName = user.name + ' ' + user.surname;
        return (
                <React.Fragment>
                    <MemberHeader conferenceId={conference.id}/>

                    <Container maxWidth="xl" style={{marginTop: '25px'}}>
                        <MainFeaturedPost title={conference.name}
                                          description={'Committee member panel, welcome ' + user_fullName + ' !'}
                                          height={'230px'}
                        />
                    </Container>

                    <Container maxWidth={'md'}>
                        <div className={'aboutConfSection'}>
                            <Typography variant="h5" component="h5" align="center" gutterBottom>
                                About conference
                            </Typography>
                            <Typography variant="body1" align={'center'} gutterBottom>
                                {conference.description}
                            </Typography>
                        </div>

                        <Typography variant="h5" component="h5" align="center" style={{marginTop: '20px'}}>
                            * * *
                        </Typography>

                        <div className={'committeeMemberSection'}>
                            <Typography variant="h5" component="h5" align="center" gutterBottom>
                                Committee member
                            </Typography>
                            <Typography variant="body1" align={'center'} gutterBottom>
                                {conference.description}
                            </Typography>

                            <Grid container align="center" style={{marginTop: '50px'}}>
                                <Grid item xs={12} sm={4} md={6}>
                                    <Typography variant="body1" gutterBottom>
                                        <b>Name:</b> {user.name}
                                        <br/>
                                        <b>Surname:</b> {user.surname}
                                        <br/>
                                        <b>Username:</b> {user.username}
                                    </Typography>
                                </Grid>
                                <Grid item xs={12} sm={4} md={6}>
                                    <Typography variant="body1" gutterBottom>
                                        <b>Email:</b> {user.email}
                                        <br/>
                                        <b>Title:</b> {user.title.name}
                                        <br/>
                                        <b>Country:</b> {user.country.name}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </div>

                        <Typography variant="h5" component="h5" align="center" style={{marginTop: '20px'}}>
                            * * *
                        </Typography>

                        <div className={'committeeMemberSectionsSection'}>
                            <Typography variant="h5" component="h5" align="center" gutterBottom>
                                Sections
                            </Typography>
                            <Typography variant="body1" align={'center'} gutterBottom>
                                Sections description.
                                Pellentesque hendrerit, eros id auctor imperdiet, enim eros vestibulum libero, sit amet posuere eros nisl ut ex.
                            </Typography>
                            <Typography variant="h6" align={'center'} gutterBottom>
                                {this.state.memberSections.map((section) => (
                                        <div>
                                            {section.name}
                                            <br/>
                                        </div>
                                ))}
                            </Typography>
                        </div>
                    </Container>

                    <Container maxWidth="lg" className="actionsContainer">
                        <Grid container>
                            <Grid item xs={12} md={6} id="actionsContainerFirstGrid">
                                <Typography variant="h5" component="h5" align="center" gutterBottom>
                                    Find applications to review
                                </Typography>
                                <div>
                                    <Typography variant="body1" align="left">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                        Morbi porta tincidunt pulvinar. Sed mattis dapibus mi, in cursus est sollicitudin eu.
                                    </Typography>
                                    <Button style={{marginTop: '20px'}}
                                            variant="outlined"
                                            color="primary"
                                            endIcon={<Icon>send</Icon>}
                                            onClick={this.handleFindAppButton}>
                                        Find
                                    </Button>
                                </div>
                            </Grid>
                            <Grid item xs={12} md={6} id="actionsContainerSecondGrid">
                                <Typography variant="h5" component="h5" align="center" gutterBottom>
                                    See your reviews
                                </Typography>
                                <div>
                                    <Typography variant="body1" align="left">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                        Morbi porta tincidunt pulvinar. Sed mattis dapibus mi, in cursus est sollicitudin eu.
                                    </Typography>
                                    <Button style={{marginTop: '20px'}}
                                            variant="outlined"
                                            size={'medium'}
                                            color="primary"
                                            endIcon={<Icon>send</Icon>}
                                            onClick={this.handleSeeReviewsButton}
                                    >
                                        See
                                    </Button>
                                </div>
                            </Grid>
                        </Grid>
                    </Container>

                </React.Fragment>
        );
    }
}

export default withRouter(CommitteeMemberHomepage);