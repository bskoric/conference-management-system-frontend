import React, {Component} from 'react';
import {Container, MenuItem, Select, Typography} from '@material-ui/core';
import {withRouter} from 'react-router-dom';
import MainFeaturedPost from '../../layout/MainFeaturedPost';
import ConferenceService from '../../../service/ConferenceService';
import {emptyConference} from '../../conference/ConferenceHelper';
import AuthService from '../../../service/AuthService';
import Grid from '@material-ui/core/Grid';
import ReviewCard from '../../review/ReviewCard';
import ReactPaginate from 'react-paginate';
import ReviewService from '../../../service/ReviewService';
import MemberHeader from '../../layout/header/MemberHeader';
import {APPLY_STATUS_APPLIED, APPLY_STATUS_CLOSED, APPLY_STATUS_DECLINED, APPLY_STATUS_IN_REVIEW} from '../../../const/Const';
import InputLabel from '@material-ui/core/InputLabel';

class MyReviewsPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            conference:  emptyConference,
            reviews:     [],
            offset:      0,
            perPage:     4,
            currentPage: 0
        }

        this.getConference = this.getConference.bind(this);
        this.getReviews = this.getReviews.bind(this);
    }

    componentDidMount() {
        const {match: {params}} = this.props;
        const conferenceId = params.id;

        this.getConference(conferenceId);
        this.getReviews(conferenceId, AuthService.getLoggedInUser().id, APPLY_STATUS_IN_REVIEW);
    }

    getConference(conferenceId) {
        ConferenceService.getConference(conferenceId).then(res => {
            this.setState({
                        conference: res.data
                    }
            );
        })
    }

    getReviews(conferenceId, committeeId, status) {
        ReviewService.getReviewsByCommitteeMemberAndApplyStatus(committeeId, conferenceId, status).then(res => {
            const data = res.data
            const slice = data.slice(this.state.offset, this.state.offset + this.state.perPage);
            this.setState({
                        reviews:   slice,
                        pageCount: Math.ceil(data.length / this.state.perPage)
                    }
            );
        })
    }

    handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * this.state.perPage;

        this.setState({
            currentPage: selectedPage,
            offset:      offset
        }, () => {
            const {match: {params}} = this.props;
            this.getReviews(params.id, AuthService.getLoggedInUser().id, APPLY_STATUS_IN_REVIEW);
        });

    };

    handleButton = (conferenceId, reviewId) => {
        this.props.history.push(
                '/conference/' + conferenceId + '/reviews/' + reviewId)
    }

    onChange = (e) => {
        const value = e.target.value;
        this.getReviews(this.state.conference.id, AuthService.getLoggedInUser().id, value);
    }

    render() {
        const {conference} = this.state;

        return (
                <React.Fragment>
                    <MemberHeader conferenceId={conference.id}/>

                    <Container maxWidth="xl" style={{marginTop: '25px'}}>
                        <MainFeaturedPost title={conference.name}
                                          description={'Reviews'}
                                          height="200px"
                        />
                    </Container>

                    <Container maxWidth={'md'}>
                        <div className={'aboutConfSection'}>
                            <Typography variant="h5" component="h5" align="center" gutterBottom>
                                See your reviews
                            </Typography>
                            <Typography variant="body1" align={'center'} gutterBottom>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Morbi porta tincidunt pulvinar. Sed mattis dapibus mi, in cursus est sollicitudin eu.
                            </Typography>
                        </div>

                    </Container>

                    <Container maxWidth="lg" style={{padding: '15px', marginTop: '30px'}}>

                        <Typography color="textPrimary" variant="h4" align={'center'}>
                            Reviews
                        </Typography>
                        <div>
                            <Grid item xs={12} md={2}>
                                <InputLabel>Status</InputLabel>
                                <Select variant={'outlined'}
                                        onChange={this.onChange}
                                        name={'status'}
                                        defaultValue={APPLY_STATUS_IN_REVIEW}
                                        style={{display: 'flex', paddingRight: '0'}}>
                                    <MenuItem value={APPLY_STATUS_IN_REVIEW}>{APPLY_STATUS_IN_REVIEW}</MenuItem>
                                    <MenuItem value={APPLY_STATUS_APPLIED}>{APPLY_STATUS_APPLIED}</MenuItem>
                                    <MenuItem value={APPLY_STATUS_DECLINED}>{APPLY_STATUS_DECLINED}</MenuItem>
                                    <MenuItem value={APPLY_STATUS_CLOSED}>{APPLY_STATUS_CLOSED}</MenuItem>
                                </Select>
                            </Grid>
                        </div>
                        <Grid container spacing={2} style={{marginTop: '30px'}}>
                            {this.state.reviews.map((review) => (
                                    <Grid item xs={12} sm={6} key={review.id}>
                                        <ReviewCard button={'Open review'}
                                                    application={review.apply}
                                                    handleButton={() => this.handleButton(this.state.conference.id, review.id)}

                                        />
                                    </Grid>
                            ))}
                        </Grid>
                        <ReactPaginate
                                previousLabel={'prev'}
                                nextLabel={'next'}
                                breakLabel={'...'}
                                breakClassName={'break-me'}
                                pageCount={this.state.pageCount}
                                marginPagesDisplayed={2}
                                pageRangeDisplayed={5}
                                onPageChange={this.handlePageClick}
                                containerClassName={'pagination'}
                                subContainerClassName={'pages pagination'}
                                activeClassName={'active'}/>
                    </Container>

                </React.Fragment>
        );
    }

}

export default withRouter(MyReviewsPage);