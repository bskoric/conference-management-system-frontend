import React, {Component} from 'react';
import {Container, Divider} from '@material-ui/core';
import ConferenceService from '../../../service/ConferenceService';
import {emptyConference} from '../../conference/ConferenceHelper';
import Typography from '@material-ui/core/Typography';
import ReviewCard from '../../review/ReviewCard';
import FileDownload from '../../document/FileDownload';
import CommentList from '../../review/CommentList';
import Button from '@material-ui/core/Button';
import {Cancel, HourglassEmpty} from '@material-ui/icons';
import Grid from '@material-ui/core/Grid';
import '../../../assets/css/review-page.css'
import CheckIcon from '@material-ui/icons/Check';
import {APPLY_STATUS_IN_REVIEW, REVIEW_STATUS_APPROVED, REVIEW_STATUS_DECLINED, REVIEW_STATUS_IN_PROGRESS} from '../../../const/Const';
import TransitionAlerts from '../../layout/TransitionAlerts';
import ReviewService from '../../../service/ReviewService';
import MemberHeader from '../../layout/header/MemberHeader';

class ReviewPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            conference:    emptyConference,
            review:        {},
            alertMessage:  '',
            errorSeverity: '',
            showAlert:     false
        }

        this.getConference = this.getConference.bind(this);
        this.getReview = this.getReview.bind(this);
    }

    componentDidMount() {
        const {match: {params}} = this.props;
        this.getConference(params.id);
        this.getReview(params.reviewId);
    }

    getConference(conferenceId) {
        ConferenceService.getConference(conferenceId).then(res => {
            this.setState({
                        conference: res.data
                    }
            );
        })
    }

    getReview(reviewId) {
        ReviewService.getReview(reviewId).then(res => {
            this.setState({
                        review: res.data
                    }
            );
        })
    }

    onClose = () => {
        this.setState({
            showAlert: false
        });
    }

    handleStatus = (status) => {
        const {match: {params}} = this.props;
        ReviewService.changeStatus(params.reviewId, status).then(res => {
            let reviewChanged = this.state.review;
            reviewChanged.status = status;
            this.setState({
                        review:        reviewChanged,
                        errorSeverity: 'success',
                        alertMessage:  'Successfully added',
                        showAlert:     true
                    }
            );
        })
    }

    render() {
        const {conference} = this.state;
        const {review} = this.state;
        const application = review.apply ? review.apply : null;
        return (
                <React.Fragment>
                    <MemberHeader conferenceId={conference.id}/>

                    {this.state.showAlert && <TransitionAlerts alertMessage={this.state.alertMessage}
                                                               severity={this.state.errorSeverity}
                                                               close={this.onClose}/>}

                    <Container maxWidth="lg" className={'mainHeader'}>
                        <Typography color="textPrimary" variant="h5">
                            {conference.name} - Review
                        </Typography>
                        <Divider/>
                    </Container>

                    <Container maxWidth="lg" className="reviewContainer">
                        {application &&
                        <>
                            <ReviewCard application={application} fullWidth/>

                            <Grid container style={{marginTop: '30px'}}>
                                <Grid item xs={12} md={6}>
                                    <div>
                                        <FileDownload documentId={application.document.id}
                                                      fileName={application.document.fileName}/>
                                    </div>
                                </Grid>

                                <Grid item xs={12} md={6}>
                                    {application.status === APPLY_STATUS_IN_REVIEW && <div className="buttonsMemberReview">
                                        <Button variant="outlined" className="declineAppBtn"
                                                onClick={() => this.handleStatus(REVIEW_STATUS_DECLINED)}>
                                            <Cancel/> Decline
                                        </Button>
                                        <Button variant="outlined" className="inProgressAppBtn"
                                                onClick={() => this.handleStatus(REVIEW_STATUS_IN_PROGRESS)}>
                                            <HourglassEmpty/> In progress
                                        </Button>
                                        <Button variant="outlined" color="default" className="approveAppBtn"
                                                onClick={() => this.handleStatus(REVIEW_STATUS_APPROVED)}>
                                            <CheckIcon/> Approve
                                        </Button>
                                    </div>}
                                </Grid>
                            </Grid>

                        </>}
                    </Container>

                    <Container maxWidth="lg" className="commentsContainer">
                        <Typography color="textPrimary" variant="h5">
                            Comments
                        </Typography>

                        <Divider/>

                        {application && <CommentList application={application}/>}

                    </Container>

                </React.Fragment>
        );
    }
}

export default ReviewPage;