import React, {Component} from 'react';
import {Container} from '@material-ui/core';
import {withRouter} from 'react-router-dom';
import MainFeaturedPost from '../../layout/MainFeaturedPost';
import UserHeader from '../../layout/header/UserHeader';
import ConferenceService from '../../../service/ConferenceService';
import {emptyConference} from '../../conference/ConferenceHelper';
import AuthService from '../../../service/AuthService';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Application from '../../application/Application';
import ReviewService from '../../../service/ReviewService';
import ReviewCard from '../../review/ReviewCard';
import {APPLY_STATUS_IN_REVIEW} from '../../../const/Const';

class ApplyRequestPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            conference:   emptyConference,
            applications: [],
            reviews:      []
        }

        this.getConference = this.getConference.bind(this);
        this.getApplications = this.getApplications.bind(this);
        this.getReview = this.getReview.bind(this);
        this.handleButton = this.handleButton.bind(this);
    }

    componentDidMount() {
        const {match: {params}} = this.props;
        const conferenceId = params.id;
        const user = AuthService.getLoggedInUser();

        this.getConference(conferenceId);
        this.getApplications(conferenceId, user.id);
        this.getReview(conferenceId, user.id);
    }

    getConference(conferenceId) {
        ConferenceService.getConference(conferenceId).then(res => {
            this.setState({
                        conference: res.data
                    }
            );
        })
    }

    getApplications(conferenceId, userId) {
        ConferenceService.getApplications(conferenceId, userId).then(res => {
            this.setState({
                        applications: res.data
                    }
            );
        })
    }

    getReview(conferenceId, userId) {
        ReviewService.getReviews(conferenceId, userId).then(res => {
            this.setState({
                        reviews: res.data
                    }
            );
        })
    }

    handleButton(conferenceId, applicationId) {
        this.props.history.push(
                '/conference/' + conferenceId + '/apply/' + applicationId + '/review')
    }

    render() {
        const {conference} = this.state;
        return (
                <React.Fragment>
                    <UserHeader conferenceId={conference.id}/>

                    <Container maxWidth="xl" style={{marginTop: '25px'}}>
                        <MainFeaturedPost title={conference.name}
                                          description={'Applications | Reviews'}
                                          height="200px"
                        />
                    </Container>

                    <Container maxWidth="lg" style={{padding: '10px'}}>
                        <Typography color="textPrimary" variant="h5">
                            Applications
                        </Typography>
                        <Grid container spacing={2}>
                            {this.state.applications.map((app) => (
                                    <Grid item xs={12} sm={6} key={app.id}>
                                        {app.status === APPLY_STATUS_IN_REVIEW && <Application application={app}/>}
                                        {app.status !== APPLY_STATUS_IN_REVIEW && <Application reviewBtn application={app}/>}
                                    </Grid>
                            ))}
                        </Grid>
                    </Container>


                    <Container maxWidth="lg" style={{padding: '10px'}}>
                        <Typography color="textPrimary" variant="h5">
                            In Review
                        </Typography>
                        <div>
                            {this.state.applications.filter((app) => app.status === APPLY_STATUS_IN_REVIEW)
                                 .map((app) => {
                                     return <ReviewCard button={'Open review'}
                                                        handleButton={() => this.handleButton(app.document.conference.id, app.id)}
                                                        application={app}/>
                                 })
                            }
                        </div>
                    </Container>

                </React.Fragment>
        );
    }

}

export default withRouter(ApplyRequestPage);