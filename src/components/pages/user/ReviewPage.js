import React, {Component} from 'react';
import {Container, Divider} from '@material-ui/core';
import UserHeader from '../../layout/header/UserHeader';
import ConferenceService from '../../../service/ConferenceService';
import {emptyConference} from '../../conference/ConferenceHelper';
import Typography from '@material-ui/core/Typography';
import ReviewCard from '../../review/ReviewCard';
import FileDownload from '../../document/FileDownload';
import CommentList from '../../review/CommentList';
import Button from '@material-ui/core/Button';
import {Cancel} from '@material-ui/icons';
import Grid from '@material-ui/core/Grid';
import '../../../assets/css/review-page.css'
import CheckIcon from '@material-ui/icons/Check';
import {APPLY_STATUS_APPLIED, APPLY_STATUS_CLOSED, APPLY_STATUS_IN_REVIEW} from '../../../const/Const';
import TransitionAlerts from '../../layout/TransitionAlerts';

class ReviewPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            conference:    emptyConference,
            application:   null,
            alertMessage:  '',
            errorSeverity: '',
            showAlert:     false
        }

        this.getConference = this.getConference.bind(this);
        this.getApplications = this.getApplications.bind(this);
        this.handleFinish = this.handleFinish.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.onClose = this.onClose.bind(this);
    }

    componentDidMount() {
        const {match: {params}} = this.props;
        this.getConference(params.id);
        this.getApplications(params.apply, params.id);
    }

    getConference(conferenceId) {
        ConferenceService.getConference(conferenceId).then(res => {
            this.setState({
                        conference: res.data
                    }
            );
        })
    }

    getApplications(applyId, conferenceId) {
        ConferenceService.getApplicationByIdAndConference(applyId, conferenceId).then(res => {
            this.setState({
                        application: res.data
                    }
            );
        })
    }

    onClose() {
        this.setState({
            showAlert: false
        });
    }

    handleFinish(applyId) {
        ConferenceService.finishApplication(this.state.application.id).then(res => {
            let app = this.state.application;
            app.status = APPLY_STATUS_APPLIED;
            this.setState({
                        application:   app,
                        errorSeverity: 'success',
                        alertMessage:  'Your application is successful. You applied for the conference',
                        showAlert:     true
                    }
            );
        }).catch((error) => {
            this.setState({
                errorSeverity: 'error',
                alertMessage:  error.response.data.errorMessage,
                showAlert:     true
            });
        })
    }

    handleClose(applyId) {
        ConferenceService.closeApplication(this.state.application.id).then(res => {
            let app = this.state.application;
            app.status = APPLY_STATUS_CLOSED;
            this.setState({
                        application:   app,
                        errorSeverity: 'warning',
                        alertMessage:  'You closed application.',
                        showAlert:     true
                    }
            );
        })
    }

    render() {
        const {conference} = this.state;
        const {application} = this.state;
        return (
                <React.Fragment>
                    <UserHeader conferenceId={conference.id}/>

                    {this.state.showAlert && <TransitionAlerts alertMessage={this.state.alertMessage}
                                                               severity={this.state.errorSeverity}
                                                               close={this.onClose}/>}

                    <Container maxWidth="lg" style={{marginTop: '25px'}}>
                        <Typography color="textPrimary" variant="h5">
                            {conference.name} - Review
                        </Typography>
                        <Divider/>
                    </Container>

                    <Container maxWidth="lg" className="reviewContainer">
                        {this.state.application &&
                        <>
                            <ReviewCard application={this.state.application} fullWidth/>

                            <Grid container style={{marginTop: '30px'}}>
                                <Grid item xs={12} md={6}>
                                    <div>
                                        <FileDownload documentId={this.state.application.document.id}
                                                      fileName={this.state.application.document.fileName}/>
                                    </div>
                                </Grid>

                                <Grid item xs={12} md={6}>
                                    {application.status === APPLY_STATUS_IN_REVIEW && <div>
                                        <Button variant="outlined" className="closeAppBtn" onClick={this.handleClose}><Cancel/> Close
                                            application</Button>
                                        <Button variant="outlined" className="deleteAppBtn"><Cancel/> Delete application</Button>
                                    </div>}
                                </Grid>
                            </Grid>

                            {application.status === APPLY_STATUS_IN_REVIEW && <div>
                                <Button variant="outlined" size="large" color="default" className="finishAppBtn"
                                        onClick={this.handleFinish}>
                                    <CheckIcon/> Finish application
                                </Button>
                            </div>}

                        </>}
                    </Container>

                    <Container maxWidth="lg" className="commentsContainer">
                        <Typography color="textPrimary" variant="h5">
                            Comments
                        </Typography>

                        <Divider/>

                        {application && <CommentList application={this.state.application}/>}

                    </Container>

                </React.Fragment>
        );
    }

}

export default ReviewPage;