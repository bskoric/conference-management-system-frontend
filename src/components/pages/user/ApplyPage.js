import React, {Component} from 'react';
import './../../../assets/css/user-apply.css'
import {Container, CssBaseline, Input, MenuItem, Select, TextField} from '@material-ui/core';
import {Redirect, withRouter} from 'react-router-dom';
import MainFeaturedPost from '../../layout/MainFeaturedPost';
import UserHeader from '../../layout/header/UserHeader';
import ConferenceService from '../../../service/ConferenceService';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import DocumentService from '../../../service/DocumentService';
import {emptyConference} from '../../conference/ConferenceHelper';
import AuthService from '../../../service/AuthService';
import TransitionAlerts from '../../layout/TransitionAlerts';
import SectionService from '../../../service/SectionService';

class ApplyPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            conference:         emptyConference,
            document:           {
                name:        '',
                description: '',
                createdAt:   '',
                fileName:    '',
                user:        null,
                conference:  null,
                category:    '',
                section:     ''
            },
            file:               '',
            documentCategories: [],
            sections:           [],
            alertMessage:       '',
            errorSeverity:      '',
            showAlert:          false
        }

        this.getConference = this.getConference.bind(this);
        this.getDocumentCategories = this.getDocumentCategories.bind(this);
        this.getSections = this.getSections.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onFileChange = this.onFileChange.bind(this);
        this.apply = this.apply.bind(this);
        this.onClose = this.onClose.bind(this);
    }

    componentDidMount() {
        const {match: {params}} = this.props;
        this.getConference(params.id);
        this.getDocumentCategories();
    }

    getConference(conferenceId) {
        ConferenceService.getConference(conferenceId).then(res => {
            this.setState({
                        conference: res.data
                    }
            );
            this.getSections(res.data.category.id)
        })
    }

    getDocumentCategories() {
        DocumentService.getCategories().then(res => {
            this.setState({
                        documentCategories: res.data
                    }
            );
        })
    }

    getSections(categoryId) {
        SectionService.getConferenceSectionsForCategory(categoryId).then(res => {
            this.setState({
                        sections: res.data
                    }
            );
        })
    }

    onChange(e) {
        const target = e.target;
        let value = e.target.value;
        const name = target.name;
        let document = {...this.state.document};
        document[name] = value;
        this.setState({document});
    }

    onFileChange(event) {
        const file = event.target.files[0];
        this.setState({
            file: file
        });
    }

    apply(e) {
        e.preventDefault();

        let {document} = this.state;
        document.user = AuthService.getLoggedInUser();
        document.conference = this.state.conference;
        document.createdAt = new Date().toLocaleDateString('en-CA');

        let formData = new FormData();
        formData.append('document', JSON.stringify(document));
        formData.append('file', this.state.file);

        if (!document.name || !document.description || !document.category || !document.section || !this.state.file) {
            this.setState({
                errorSeverity: 'error',
                alertMessage:  'All fields are mandatory',
                showAlert:     true
            });
            return;
        }

        ConferenceService.applyToConference(formData).then((response) => {

            this.setState({
                errorSeverity: 'success',
                alertMessage:  'Your application is created !',
                showAlert:     true
            });
            setTimeout(() => {
                this.setState({
                    redirect: true
                })
            }, 1500);

        }).catch((error) => {
            this.setState({
                errorSeverity: 'error',
                alertMessage:  error.response.data.errorMessage,
                showAlert:     true
            });
        })
    }

    onClose() {
        this.setState({
            showAlert: false
        });
    }

    render() {
        const {conference} = this.state;
        const user = AuthService.getLoggedInUser();
        if (this.state.redirect) {
            return (<Redirect to={'/conference/' + conference.id}/>);
        }

        return (
                <React.Fragment>
                    <UserHeader conferenceId={conference.id}/>

                    {this.state.showAlert && <TransitionAlerts alertMessage={this.state.alertMessage}
                                                               severity={this.state.errorSeverity}
                                                               close={this.onClose}/>}

                    <Container maxWidth="xl" style={{marginTop: '25px'}}>
                        <MainFeaturedPost title={conference.name}
                                          description={'ApplyPage'}
                                          height="200px"
                        />
                    </Container>

                    <Container maxWidth="xl">
                        <Grid container>
                            <CssBaseline/>
                            <Grid item xs={false} sm={4} md={6}>
                                <div className={'paperText'}>
                                    <Typography variant="h5" component="h5" gutterBottom align="center">
                                        Apply for the conference
                                    </Typography>
                                    <Typography variant="body1" gutterBottom>
                                        Fill in application form, and upload your work document.
                                        Our committee member will review it, verify and left comments.
                                        <br/>
                                        <br/>
                                        Morbi mi arcu, finibus id interdum sit amet, sodales nec elit.
                                        Pellentesque hendrerit, eros id auctor imperdiet, enim eros vestibulum libero,
                                        sit amet posuere eros nisl ut ex. Sed lacinia arcu non justo mollis egestas.
                                        Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                                    </Typography>
                                    <div className={'user-info'}>
                                        <Typography variant="h5" component="h5" gutterBottom align="center">
                                            User
                                        </Typography>
                                        <Grid container>
                                            <Grid item xs={12} sm={4} md={6}>
                                                <Typography variant="body1" gutterBottom>
                                                    <b>Name:</b> {user.name}
                                                    <br/>
                                                    <b>Surname:</b> {user.surname}
                                                    <br/>
                                                    <b>Username:</b> {user.username}
                                                </Typography>
                                            </Grid>
                                            <Grid item xs={12} sm={4} md={6}>
                                                <Typography variant="body1" gutterBottom>
                                                    <b>Email:</b> {user.email}
                                                    <br/>
                                                    <b>Title:</b> {user.title.name}
                                                    <br/>
                                                    <b>Country:</b> {user.country.name}
                                                </Typography>
                                            </Grid>
                                        </Grid>
                                    </div>
                                </div>
                            </Grid>
                            <Grid item xs={12} sm={8} md={6}>
                                <div className={'paperForm'}>
                                    <Typography component="h1" variant="h5">
                                        <p style={{textAlign: 'center'}}>Create application</p>
                                    </Typography>
                                    <form noValidate method="post">
                                        <Grid container spacing={2}>
                                            <Grid item xs={12} sm={12}>
                                                <TextField
                                                        autoComplete="name"
                                                        name="name"
                                                        variant="outlined"
                                                        required
                                                        fullWidth
                                                        id="name"
                                                        label="Document Name"
                                                        autoFocus
                                                        onChange={this.onChange}
                                                />
                                            </Grid>
                                            <Grid item xs={12} style={{marginTop: '20px'}}>
                                                <TextField
                                                        id="outlined-multiline-static"
                                                        label="Document description"
                                                        name="description"
                                                        multiline
                                                        rows={4}
                                                        defaultValue=""
                                                        variant="outlined"
                                                        onChange={this.onChange}
                                                />
                                            </Grid>
                                            <Grid item xs={12}>
                                                <InputLabel>Upload document</InputLabel>
                                                <Input type="file"
                                                       id="file"
                                                       name="file"
                                                       onChange={this.onFileChange}
                                                />
                                            </Grid>
                                            <Grid item xs={6}>
                                                <InputLabel>Document category</InputLabel>
                                                <Select variant={'outlined'}
                                                        onChange={this.onChange}
                                                        name={'category'}
                                                        style={{display: 'inherit', paddingRight: '0'}}>
                                                    {this.state.documentCategories.map(
                                                            cat => <MenuItem key={cat.id} value={cat}>{cat.name}</MenuItem>)}
                                                </Select>
                                            </Grid>
                                            <Grid item xs={6}>
                                                <InputLabel>Section</InputLabel>
                                                <Select variant={'outlined'}
                                                        onChange={this.onChange}
                                                        style={{display: 'inherit', paddingRight: '0'}}
                                                        name={'section'}
                                                >
                                                    {this.state.sections.map(
                                                            section => <MenuItem key={section.id}
                                                                                 value={section}>{section.name}</MenuItem>)}
                                                </Select>
                                            </Grid>
                                        </Grid>
                                        <Button
                                                type="submit"
                                                fullWidth
                                                variant="contained"
                                                color="primary"
                                                className={'submitBtn'}
                                                onClick={this.apply}
                                        >
                                            Apply
                                        </Button>
                                    </form>
                                </div>
                            </Grid>
                        </Grid>
                    </Container>
                </React.Fragment>
        );
    }
}

export default withRouter(ApplyPage);