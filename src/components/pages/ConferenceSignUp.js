import React, {Component} from 'react';
import './../../assets/css/homepage.css';
import {Button, Container} from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import FeaturedPost from '../layout/FeaturedPost';
import MainFeaturedPost from '../layout/MainFeaturedPost';
import Typography from '@material-ui/core/Typography';
import ConferenceService from '../../service/ConferenceService';
import AuthService from '../../service/AuthService';
import TransitionAlerts from '../layout/TransitionAlerts';
import Header from '../layout/header/Header';

class ConferenceSignUp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            pendingRegistration: props.pendingRegistration,
            alertMessage:        '',
            errorSeverity:       '',
            showAlert:           false
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.onCloseAlert = this.onCloseAlert.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        ConferenceService.registerToConferenceApp(this.props.conference.id, AuthService.getLoggedInUser().id).then((response) => {
            this.setState({
                pendingRegistration: true,
                errorSeverity:       'success',
                alertMessage:        'Your application is sent !',
                showAlert:           true
            });
        }).catch((error) => {
            this.setState({
                pendingRegistration: false,
                errorSeverity:       'error',
                alertMessage:        'Error, try again',
                showAlert:           true
            });
        });
    }

    onCloseAlert() {
        this.setState({
            showAlert: false
        });
    }

    render() {
        const conference = this.props.conference;
        const where = conference.town + ', ' + conference.country.name;
        const showStatusAreaStyle = {
            backgroundColor: '#e8e8e8',
            padding:         '15px',
            borderTop:       '1px solid #dfdfdf',
            borderBottom:    '1px solid #dfdfdf',
            marginTop:       '30px'
        };

        return (
                <>
                    <header className="App-header">
                        <Header sections={[]}/>
                    </header>

                    {this.state.showAlert && <TransitionAlerts
                            alertMessage={this.state.alertMessage}
                            severity={this.state.errorSeverity}
                            close={this.onCloseAlert}/>}

                    <Container maxWidth="xl" style={{marginTop: '25px'}}>
                        <MainFeaturedPost title={conference.name}
                                          description={'Sign up'}
                                          image={'https://images.unsplash.com/photo-1516979187457-637abb4f9353?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80'}
                                          button={!this.state.pendingRegistration}
                                          buttonText={'Register'}
                                          handleClick={this.handleSubmit}
                        />
                    </Container>

                    <Container maxWidth="md" style={{marginTop: '20px'}}>
                        <Typography variant="h5" component="h5" align="center" gutterBottom>
                            About conference
                        </Typography>
                        <Typography variant="body1" align="center">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Morbi porta tincidunt pulvinar. Sed mattis dapibus mi, in cursus est sollicitudin eu.
                        </Typography>
                    </Container>

                    <Container maxWidth="lg">
                        <Grid container spacing={4} style={{marginTop: '50px'}}>
                            <FeaturedPost title={'Where ?'}
                                          description={where}
                                          image={'https://images.unsplash.com/photo-1526778548025-fa2f459cd5c1?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1033&q=80'}/>

                            <FeaturedPost title={'When ?'}
                                          description={conference.date}
                                          image={'https://static.coolgift.com/media/cache/sylius_shop_product_large_thumbnail/product/Backwards-Clock-Round-2.jpg'}/>

                            <FeaturedPost title={'What ?'}
                                          description={conference.category.name}
                                          image={'https://static.magento.com/sites/default/files/cu_images/innovations-lab_0.png'}/>
                        </Grid>
                    </Container>

                    <Container maxWidth="xl" style={showStatusAreaStyle}>
                        {!this.state.pendingRegistration &&
                        <Typography variant="h4" align="center" color="textPrimary" paragraph>
                            Register to conference
                            <br/>
                            <Button size="large"
                                    variant="outlined"
                                    style={{marginTop: '20px'}}
                                    onClick={this.handleSubmit}>
                                Register
                            </Button>
                        </Typography>
                        }

                        {this.state.pendingRegistration &&
                        <>
                            <Typography variant="h4" align="center" color="textPrimary" paragraph>
                                Pending registration to conference
                            </Typography>
                            <Typography variant="h5" align="center" color="textPrimary" paragraph>
                                You application for register to conference app is in process...
                            </Typography>
                        </>
                        }
                    </Container>
                </>
        );
    }
}

export default ConferenceSignUp;