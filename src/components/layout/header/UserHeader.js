import React from 'react';
import Header from './Header';

export default function UserHeader(props) {
    const navigation = [
        {title: 'Conference home', url: '/conference/' + props.conferenceId},
        {title: 'Apply', url: '/conference/' + props.conferenceId + '/apply'},
        {title: 'Application request', url: '/conference/' + props.conferenceId + '/apply/request'}
    ];

    return (
            <header className="App-header">
                <Header sections={navigation}/>
            </header>
    );
}

UserHeader.propTypes = {};