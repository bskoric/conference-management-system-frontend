import React, {Component} from 'react';
import '../../../assets/css/header.css';
import AuthService from '../../../service/AuthService';

/*
Downloaded form codepen.io -> https://codepen.io/yellibeanz
*/
class Header extends Component {
    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);
        this.handleOutsideClick = this.handleOutsideClick.bind(this);
        this.sidebarClick = this.sidebarClick.bind(this);
        this.navbarScroll = this.navbarScroll.bind(this);

        this.state = {
            sidenavVisible: false
        };
    }

    handleClick() {
        var hamburger = document.getElementById('hamburger');
        var sidenav = document.getElementById('mySidenav');
        var menuoverlay = document.getElementById('menu-overlay');
        if (!this.state.sidenavVisible) {
            sidenav.classList.add('show');
            hamburger.classList.add('close');
            menuoverlay.classList.add('active');
            document.addEventListener('click', this.handleOutsideClick, false);
        } else {

            document.removeEventListener('click', this.handleOutsideClick, false);
            sidenav.classList.remove('show');
            hamburger.classList.remove('close');
            menuoverlay.classList.remove('active');
        }

        this.setState(prevState => ({
            sidenavVisible: !prevState.sidenavVisible
        }));
    }

    handleOutsideClick(e) {
        // ignore clicks on the component itself
        if (this.node.contains(e.target)) {
            return;
        }
        this.handleClick();
    }

    sidebarClick() {
        document.getElementById('mySidenav').addEventListener('click', function (event) {
            event.stopPropagation()
        })
    }

    navbarScroll() {
        window.addEventListener('scroll', function () {
            var header = document.getElementById('main-header');
            if (document.body.scrollTop > 60 || document.documentElement.scrollTop > 60) {
                header.classList.add('minimize');
            } else {
                header.classList.remove('minimize');
            }
        });
    }

    render() {
        const {sections} = this.props;
        return (
                <header className="main-header" id="main-header" ref={node => {
                    this.node = node;
                }} onLoad={this.navbarScroll}>
                    <div className="hamburger-wrapper">
                        <button type="button" className="hamburger" id="hamburger" onClick={this.handleClick}>
     <span className="hamburger-text-wrap">
       <span className="hamburger-text-open hamburger-text">menu</span>
<span className="hamburger-text-close hamburger-text">close</span>
     </span>
                            <span className="hamburger-line-wrap">
        <span className="hamburger-line hamburger-line-1"></span>
<span className="hamburger-line hamburger-line-2"></span>
<span className="hamburger-line hamburger-line-3"></span>
     </span>
                        </button>
                    </div>
                    <div className="container">
                        <div className="logo-and-menu-container">
                            <div id="menu-overlay"></div>
                            <div className="logo-column">
                                <div className="logo-wrapper">
                                    <a href="/" className="header-logo logo-image">
                                        <h1>Confms</h1>
                                    </a>
                                </div>
                            </div>
                            <div className="menu-column">
                                <div className="standard-menu-container">
                                    <nav>
                                        <div className="sidenav-container" id="mySidenav" onMouseOver={this.sidebarClick}>
                                            <div className="sidenav-container-inner">
                                                <ul>
                                                    {sections.map((section) => (
                                                      <li className="menu-item"><a href={section.url}>{section.title}</a></li>
                                                    ))}
                                                    <li className="menu-item">
                                                        <a href=""
                                                           style={{color : "#8f1111"}}
                                                           onClick={AuthService.logout}>
                                                            Logout ({AuthService.getLoggedInUsername()})
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div id="menu-main" className="menu-main">
                                            <ul>
                                                {sections.map((section) => (
                                                        <li className="menu-item"><a href={section.url}>{section.title}</a></li>
                                                ))}
                                                <li className="menu-item">
                                                    <a href=""
                                                       style={{color : "#8f1111"}}
                                                       onClick={AuthService.logout}>
                                                        Logout ({AuthService.getLoggedInUsername()})
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </header>
        );
    }
}

export default Header;