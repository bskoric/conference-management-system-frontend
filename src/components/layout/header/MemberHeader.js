import React from 'react';
import Header from './Header';

export default function MemberHeader(props) {
    const navigation = [
        {title: 'Home', url: '/conference/' + props.conferenceId},
        {title: 'Find applications', url: '/conference/' + props.conferenceId + '/applications'},
        {title: 'My reviews', url: '/conference/' + props.conferenceId + '/reviews'}
    ];

    return (
            <header className="App-header">
                <Header sections={navigation}/>
            </header>
    );
}

MemberHeader.propTypes = {};