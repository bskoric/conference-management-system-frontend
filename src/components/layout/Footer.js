import React from 'react';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

const useStyles = makeStyles((theme) => ({
    footer: {
        marginTop: 'auto',
        backgroundColor:
                   theme.palette.type === 'dark' ? theme.palette.primary : theme.palette.grey[800],
        width:     '100%'
    }
}));

export default function Footer() {
    const classes = useStyles();

    return (
            <footer className={classes.footer}>
                <Container maxWidth="sm">
                    <Typography variant="body1" align="center">
                        Confms | Copyright © FON 2021
                    </Typography>
                </Container>
            </footer>
    );
}