import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import {Divider} from '@material-ui/core';
import Button from '@material-ui/core/Button';

const defaultImage = 'https://images.unsplash.com/photo-1516979187457-637abb4f9353?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80';

const useStyles = makeStyles((theme) => ({
    mainFeaturedPost:        {
        position:           'relative',
        backgroundColor:    theme.palette.grey[800],
        color:              theme.palette.common.white,
        marginBottom:       theme.spacing(4),
        backgroundSize:     'cover',
        backgroundRepeat:   'no-repeat',
        backgroundPosition: 'center'
    },
    overlay:                 {
        position:                     'absolute',
        top:                          0,
        bottom:                       0,
        right:                        0,
        left:                         0,
        backgroundColor:              'rgba(0,0,0,.3)',
        ['@media (max-width:780px)']: {
            backgroundColor: 'rgba(0,0,0,.6)'
        }
    },
    mainFeaturedPostContent: {
        position:                     'relative',
        padding:                      theme.spacing(3),
        [theme.breakpoints.up('md')]: {
            padding:      theme.spacing(6),
            paddingRight: 0
        }
    }
}));

export default function MainFeaturedPost(props) {
    const classes = useStyles();
    const {title, description, imageTitle, button, buttonText, handleClick} = props;
    const image = props.image ? props.image : defaultImage;
    const featurePostHeight = props.height ? props.height : '280px';
    const CustomColorTextTypography = withStyles({
        root: {
            color:                        'rgba(0, 0, 0, 0.87)',
            ['@media (max-width:780px)']: {
                color: 'rgb(255 253 253 / 87%)'
            }
        }
    })(Typography);
    return (
            <Paper className={classes.mainFeaturedPost} style={{backgroundImage: `url(${image})`, height: `${featurePostHeight}`}}>
                {<img style={{display: 'none'}} src={image} alt={imageTitle}/>}
                <div className={classes.overlay}/>
                <Grid container>
                    <Grid item md={6}>
                        <div className={classes.mainFeaturedPostContent}>
                            <CustomColorTextTypography component="h2" variant="h4" color="textPrimary" gutterBottom>
                                {title}
                            </CustomColorTextTypography>
                            <Divider/>
                            <CustomColorTextTypography variant="h5" color="textPrimary" paragraph
                                                       style={{marginTop: '10px', marginBottom: '20px'}}>
                                {description}
                            </CustomColorTextTypography>
                            {button && <Button
                                    variant="contained"
                                    color="primary"
                                    onClick={handleClick}>{buttonText}</Button>}
                        </div>
                    </Grid>
                </Grid>
            </Paper>
    );
}

MainFeaturedPost.propTypes = {
    title:       PropTypes.string,
    description: PropTypes.string,
    image:       PropTypes.string,
    imageTitle:  PropTypes.string,
    button:      PropTypes.string,
    buttonText:  PropTypes.string,
    handleClick: PropTypes.func
};