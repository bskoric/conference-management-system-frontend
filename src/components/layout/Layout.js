import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';
import ConferenceHomepage from '../pages/ConferenceHomepage';
import Footer from './Footer';
import ApplyPage from '../pages/user/ApplyPage';
import ApplyRequestPage from '../pages/user/ApplyRequestPage';
import ReviewPage from '../pages/user/ReviewPage';
import ReviewPageCommittee from '../pages/committee-member/ReviewPage';
import ConferenceAuthenticatedRoute from '../auth/ConferenceAuthenticatedRoute';
import FindApplicationPage from '../pages/committee-member/FindApplicationPage';
import MyReviewsPage from '../pages/committee-member/MyReviewsPage';

class Layout extends Component {
    render() {
        return (
                <>
                    <div className="container-app">
                        <div className="wrapper">
                            <section className="content-app">
                                <Switch>
                                    <Route exact path="/conference/:id(\d+)" component={ConferenceHomepage}/>

                                    <ConferenceAuthenticatedRoute user exact
                                                                  path="/conference/:id(\d+)/apply"
                                                                  component={ApplyPage}/>
                                    <ConferenceAuthenticatedRoute user exact
                                                                  path="/conference/:id(\d+)/apply/request"
                                                                  component={ApplyRequestPage}/>
                                    <ConferenceAuthenticatedRoute user exact
                                                                  path="/conference/:id(\d+)/apply/:apply(\d+)/review"
                                                                  component={ReviewPage}/>

                                    <ConferenceAuthenticatedRoute member exact
                                                                  path="/conference/:id(\d+)/applications"
                                                                  component={FindApplicationPage}/>

                                    <ConferenceAuthenticatedRoute member exact
                                                                  path="/conference/:id(\d+)/reviews"
                                                                  component={MyReviewsPage}/>

                                    <ConferenceAuthenticatedRoute member exact
                                                                  path="/conference/:id(\d+)/reviews/:reviewId(\d+)"
                                                                  component={ReviewPageCommittee}/>
                                </Switch>
                            </section>
                            <Footer/>
                        </div>
                    </div>
                </>
        );
    }
}

export default Layout;
