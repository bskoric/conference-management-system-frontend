import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Hidden from '@material-ui/core/Hidden';
import {Divider} from '@material-ui/core';

const useStyles = makeStyles({
    card:        {
        display: 'flex'
    },
    cardDetails: {
        flex: 1
    },
    cardMedia:   {
        width: 160
    }
});

export default function FeaturedPost(props) {
    const classes = useStyles();
    const {title, description, image, imageTitle, link} = props;

    return (
            <Grid item xs={12} md={6}>
                <CardActionArea component="a" href={link}>
                    <Card className={classes.card}>
                        <div className={classes.cardDetails}>
                            <CardContent>
                                <Typography component="h5" variant="h5" color="textPrimary">
                                    {title}
                                </Typography>
                                <Divider/>
                                <Typography variant="h6" component="h6" color="textSecondary">
                                    {description}
                                </Typography>
                            </CardContent>
                        </div>
                        <Hidden xsDown>
                            <CardMedia className={classes.cardMedia} image={image} title={imageTitle}/>
                        </Hidden>
                    </Card>
                </CardActionArea>
            </Grid>
    );
}

FeaturedPost.propTypes = {
    post: PropTypes.object
};