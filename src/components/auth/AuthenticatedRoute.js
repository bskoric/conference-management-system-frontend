import React, {Component} from 'react';
import {Redirect, Route} from 'react-router-dom'
import AuthService from '../../service/AuthService';

class AuthenticatedRoute extends Component {
    render() {
        if (this.props.login === true) {
            if (AuthService.isUserLoggedIn()) {
                return <Redirect to="/"/>
            }
            return <Route {...this.props} />
        }

        if (AuthService.isUserLoggedIn()) {
            return <Route {...this.props} />
        } else {
            return <Redirect to="/login"/>
        }

    }
}

export default AuthenticatedRoute