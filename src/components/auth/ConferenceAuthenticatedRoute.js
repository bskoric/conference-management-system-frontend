import React, {Component} from 'react';
import {Redirect, Route} from 'react-router-dom'
import AuthService from '../../service/AuthService';
import ConferenceService from '../../service/ConferenceService';
import {ROLE_MEMBER, ROLE_USER, USER_STATUS_PENDING} from '../../const/Const';

class ConferenceAuthenticatedRoute extends Component {

    constructor(props) {
        super(props);
        this.state = {
            userRegisterToConferenceApp: true,
            userStatus: ""
        }
    }

    componentDidMount() {
        const {computedMatch: {params}} = this.props;

        ConferenceService.isMemberOfConference(params.id, AuthService.getLoggedInUser().id).then(res => {
            if (res.data.userRegisterToConferenceApp) {
                this.setState({
                    userRegisterToConferenceApp: true,
                    userStatus:                  res.data.status
                })
            } else {
                this.setState({
                    userRegisterToConferenceApp: false,
                    userStatus:                  res.data.status
                })
            }
        });
    }

    render() {
        const roleName = AuthService.getLoggedInUser().role.name;

        if (this.props.user && roleName === ROLE_MEMBER) {
            return <Redirect to="/denied"/>
        }

        if (this.props.member && roleName === ROLE_USER) {
            return <Redirect to="/denied"/>
        }

        if (this.state.userRegisterToConferenceApp && this.state.userStatus !== USER_STATUS_PENDING) {
            return <Route {...this.props}/>
        } else {
            return <Redirect to="/denied"/>
        }
    }
}

export default ConferenceAuthenticatedRoute