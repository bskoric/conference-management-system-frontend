import React, {Component} from 'react';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import {Redirect} from 'react-router-dom';
import {REGISTER_URL} from '../../const/Const';
import Alert from '@material-ui/lab/Alert';
import AuthService from '../../service/AuthService';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import TitleService from '../../service/TitleService';
import CountryService from '../../service/CountryService';

class Register extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user:               {
                name:     '',
                surname:  '',
                email:    '',
                username: '',
                password: '',
                title:    '',
                country:  ''
            },
            titles:             [],
            countries:          [],
            hasRegisterFailed:  false,
            showSuccessMessage: false,
            redirect:           false,
            alertMessage:       ''
        };
        this.register = this.register.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    componentDidMount() {
        TitleService.getAll().then(res => {
            this.setState({titles: res.data});
        })

        CountryService.getAll().then(res => {
            this.setState({countries: res.data});
        })
    }

    onShowAlert = () => {
        this.setState({showAlert: true}, () => {
            window.setTimeout(() => {
                this.setState({showAlert: false})
            }, 3000)
        });
    };

    showAlert = (message, registerFailed) => {
        this.setState({
            hasRegisterFailed: registerFailed,
            alertMessage:      message
        });
        this.onShowAlert();
    }

    register(e) {
        e.preventDefault();
        const {user} = this.state;

        if (!user.username || !user.email || !user.password || !user.name || !user.surname || !user.title || !user.country) {
            this.showAlert("All fields are mandatory!", true);
            return;
        }

        if (this.state.user.repeatPassword !== this.state.user.password) {
            this.showAlert("Passwords do not match", true);
            return;
        }

        AuthService.register(user, REGISTER_URL).then((response) => {

           this.showAlert("User registered !", false);

            setTimeout(() => {
                this.setState({
                    redirect: true
                })
            }, 3000);

        }).catch((error) => {
            this.setState({
                hasRegisterFailed: true,
                redirect:          false,
                alertMessage:      error.response.data.errorMessage
            });
            this.onShowAlert();
        })
    }

    onChange(e) {
        const target = e.target;
        let value = e.target.value;
        const name = target.name;
        if (name === 'password' || name === 'repeatPassword') {
            value = window.btoa(e.target.value)
        }
        let user = {...this.state.user};
        user[name] = value;
        this.setState({user});
    }

    render() {

        if (this.state.redirect) {
            return (<Redirect to={'/login'}/>);
        }

        const paper = {
            marginTop:     64,
            display:       'flex',
            flexDirection: 'column',
            alignItems:    'center'
        };

        const form = {
            width:     '100%',
            marginTop: 8
        };

        const submit = {
            marginTop:    24,
            marginBottom: 16
        };

        const avatar = {
            margin:          8,
            backgroundColor: 'rgb(220, 0, 78)'
        };

        return (
                <Container component="main" maxWidth="xs">

                    {this.state.hasRegisterFailed === true && this.state.showAlert &&
                    <Alert severity="error">{this.state.alertMessage}</Alert>}

                    {this.state.hasRegisterFailed === false && this.state.showAlert &&
                    <Alert severity="success">{this.state.alertMessage}</Alert>}

                    <CssBaseline/>
                    <div style={paper}>
                        <Avatar style={avatar}>
                            <LockOutlinedIcon/>
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Confms
                            <hr/>
                            <p style={{textAlign: 'center'}}>Sign up</p>
                        </Typography>
                        <form style={form} noValidate method="post">
                            <Grid container spacing={2}>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                            autoComplete="name"
                                            name="name"
                                            variant="outlined"
                                            required
                                            fullWidth
                                            id="name"
                                            label="First Name"
                                            autoFocus
                                            onChange={this.onChange}
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                            variant="outlined"
                                            required
                                            fullWidth
                                            id="surname"
                                            label="Last Name"
                                            name="surname"
                                            autoComplete="surname"
                                            onChange={this.onChange}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                            variant="outlined"
                                            required
                                            fullWidth
                                            id="username"
                                            label="Username"
                                            name="username"
                                            onChange={this.onChange}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                            variant="outlined"
                                            required
                                            fullWidth
                                            id="email"
                                            label="Email Address"
                                            name="email"
                                            autoComplete="email"
                                            onChange={this.onChange}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                            variant="outlined"
                                            required
                                            fullWidth
                                            name="password"
                                            label="Password"
                                            type="password"
                                            id="password"
                                            autoComplete="current-password"
                                            onChange={this.onChange}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                            variant="outlined"
                                            required
                                            fullWidth
                                            name="repeatPassword"
                                            label="Repeat Password"
                                            type="password"
                                            id="repeatPassword"
                                            autoComplete="re-current-password"
                                            onChange={this.onChange}
                                    />
                                </Grid>
                                <Grid item xs={6}>
                                    <InputLabel>Title</InputLabel>
                                    <Select variant={'outlined'}
                                            onChange={this.onChange}
                                            name={"title"}
                                            style={{display: 'inherit', paddingRight: '0'}}
                                    >
                                        {this.state.titles.map(title => <MenuItem key={title.id} value={title}>{title.name}</MenuItem>)}
                                    </Select>
                                </Grid>
                                <Grid item xs={6}>
                                    <InputLabel>Country</InputLabel>
                                    <Select variant={'outlined'}
                                            onChange={this.onChange}
                                            style={{display: 'inherit', paddingRight: '0'}}
                                            name={"country"}
                                    >
                                        {this.state.countries.map(
                                                country => <MenuItem key={country.id} value={country}>{country.name}</MenuItem>)}
                                    </Select>
                                </Grid>
                            </Grid>
                            <Button
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    style={submit}
                                    onClick={this.register}
                            >
                                Sign Up
                            </Button>
                            <Grid container justify="flex-end">
                                <Grid item>
                                    <Link href="/login" variant="body2">
                                        Already have an account? Sign in
                                    </Link>
                                </Grid>
                            </Grid>
                        </form>
                    </div>
                </Container>
        );
    }
}

export default Register;