export const emptyConference = {
    id:       0,
    name:     '',
    date:     '',
    town:     '',
    country:  {
        id:      0,
        name:    '',
        isoCode: ''
    },
    category: {
        id:          0,
        name:        '',
        description: ''
    },
    closed:   false
};