import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Link from '@material-ui/core/Link';
import DocumentService from '../../service/DocumentService';
import Button from '@material-ui/core/Button';

class FileDownload extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fileName:   '',
            documentId: ''
        };
    }

    componentDidMount() {
        this.setState({
            documentId: this.props.documentId,
            fileName:   this.props.fileName
        })
    }

    onClickDownload = (event) => {
        event.preventDefault();
        const {documentId} = this.state;
        const {fileName} = this.state;
        if (documentId) {
            DocumentService.getFile(documentId).then(res => {
                const blob = new Blob([res.data], {type: 'application/octet-stream'});
                const link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = fileName;
                link.click();
            })
        }
    };

    render() {
        return <Button color="primary" variant="outlined" onClick={this.onClickDownload}>Download document</Button>
    }
}

FileDownload.propTypes = {
    application: PropTypes.object
};

export default FileDownload;