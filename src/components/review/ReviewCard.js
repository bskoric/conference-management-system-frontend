import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {Chip, Divider} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import '../../assets/css/application.css';
import {REVIEW_STATUS_APPROVED, REVIEW_STATUS_DECLINED} from '../../const/Const';
import SectionService from '../../service/SectionService';
import DocumentService from '../../service/DocumentService';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import CheckIcon from '@material-ui/icons/Check';
import {HourglassEmpty} from '@material-ui/icons';
import ReviewService from '../../service/ReviewService';
import PropTypes from 'prop-types';
import {withRouter} from 'react-router-dom';

class ReviewCard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            reviews:          [],
            section:          {},
            documentCategory: {},
            committeeMember:  {},
            alertMessage:     '',
            errorSeverity:    '',
            showAlert:        false
        }

        this.getSection = this.getSection.bind(this);
        this.getDocumentCategory = this.getDocumentCategory.bind(this);
        this.getReviews = this.getReviews.bind(this);
    }

    componentDidMount() {
        const {document} = this.props.application;
        this.getSection(document.section.id)
        this.getDocumentCategory(document.category.id)
        this.getReviews(this.props.application.id);
    }

    componentWillReceiveProps(nextProps) {
        this.getReviews(nextProps.application.id)
    }

    getSection(sectionId) {
        SectionService.getSection(sectionId).then(res => {
            this.setState({
                        section: res.data
                    }
            );
        })
    }

    getDocumentCategory(catId) {
        DocumentService.getCategory(catId).then(res => {
            this.setState({
                        documentCategory: res.data
                    }
            );
        })
    }

    getReviews(id) {
        ReviewService.getReviewsByApplication(id).then(res => {
            this.setState({
                        reviews: res.data
                    }
            );
        })
    }

    render() {
        const {application} = this.props;
        const {document} = this.props.application;
        const localDate = new Date(document.createdAt).toLocaleDateString('en-US', {year: 'numeric', month: 'long', day: 'numeric'});

        function getIconStatus(reviewStatus) {
            if (reviewStatus === REVIEW_STATUS_APPROVED) {
                return <CheckIcon/>
            } else if (reviewStatus === REVIEW_STATUS_DECLINED) {
                return <HighlightOffIcon/>;
            } else {
                return <HourglassEmpty/>
            }
        }

        const fullWidth = this.props.fullWidth ? {maxWidth: '100%'} : {};
        return (
                <>
                    <div className="application-wrapper" style={fullWidth}>
                        <div style={{marginBottom: '10px'}}>
                            <Grid container alignItems="center">
                                <Grid item xs>
                                    <Typography gutterBottom variant="h4">
                                        {document.name}
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Typography gutterBottom variant="h6">
                                        {localDate}
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Typography variant="h6">
                                {application.status}
                            </Typography>
                            <Typography color="textPrimary" variant="body1">
                                {document.description}
                            </Typography>
                        </div>
                        <Divider/>
                        <div style={{marginTop: '10px', marginBottom: '10px'}}>
                            <div style={{marginTop: '10px'}}>
                                <Chip label={this.state.section.name}/>
                                <Chip color="primary" label={this.state.documentCategory.name}/>
                            </div>
                        </div>
                        <Divider/>
                        <div style={{marginTop: '10px', marginBottom: '10px'}}>
                            <Typography color="textPrimary" variant="body2">
                                Reviewers
                            </Typography>
                            <div style={{marginTop: '10px'}}>
                                {this.state.reviews.map((review) => (
                                        <Chip key={review.id}
                                              variant={'outlined'}
                                              color="secondary"
                                              size={'medium'}
                                              icon={getIconStatus(review.status)}
                                              label={review.committeeMember.name + ' ' + review.committeeMember.surname}/>
                                ))}

                            </div>
                        </div>
                        <div style={{marginTop: '20px'}}>
                            {this.props.button && <Button color="primary" variant="text"
                                                          onClick={this.props.handleButton}>{this.props.button}</Button>}
                        </div>
                    </div>
                </>
        );
    }
}

ReviewCard.propTypes = {
    application: PropTypes.object,
    fullWidth:   PropTypes.bool,
    button:      PropTypes.bool
};

export default withRouter(ReviewCard);