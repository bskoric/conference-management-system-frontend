import React, {Component} from 'react';
import {TextField} from '@material-ui/core';
import '../../assets/css/comment.css';
import PropTypes from 'prop-types';
import UserService from '../../service/UserService';
import Button from '@material-ui/core/Button';
import TransitionAlerts from '../layout/TransitionAlerts';
import AuthService from '../../service/AuthService';
import moment from 'moment/moment';
import CommentService from '../../service/CommentService';

class AddComment extends Component {

    constructor(props) {
        super(props);
        this.state = {
            comment:       {
                text:  '',
                apply: this.props.application,
                user:  AuthService.getLoggedInUser()
            },
            alertMessage:  '',
            errorSeverity: '',
            showAlert:     false
        }

        this.getUser = this.getUser.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onClose = this.onClose.bind(this);
    }

    componentDidMount() {
    }

    getUser(userId) {
        UserService.getUser(userId).then(res => {
            this.setState({
                        user: res.data
                    }
            );
        })
    }

    handleSubmit(event) {
        event.preventDefault();

        let {comment} = this.state;

        if (!comment.text || comment.text.trim() === '') {
            this.setState({
                errorSeverity: 'error',
                alertMessage:  'Please, insert your comment',
                showAlert:     true
            });
            return;
        }

        comment.dateTime = moment().format('YYYY-MM-DD HH:mm:ss');

        CommentService.addComment(this.props.application.id, this.state.comment).then((response) => {
            this.state.comment.text = '';
            this.setState({
                errorSeverity: 'success',
                alertMessage:  'Comment is sent !',
                showAlert:     true
            });
            this.props.onCommentAdded(response.data)
        }).catch((error) => {
            this.setState({
                errorSeverity: 'error',
                alertMessage:  'Error, try again',
                showAlert:     true
            });
        });
    }

    onChange(e) {
        const target = e.target;
        let value = e.target.value;
        const name = target.name;
        let {comment} = this.state;
        comment[name] = value;
        this.setState({comment});
    }

    onClose() {
        this.setState({
            showAlert: false
        });
    }

    render() {
        return (
                <>
                    {this.state.showAlert && <TransitionAlerts alertMessage={this.state.alertMessage}
                                                               severity={this.state.errorSeverity}
                                                               close={this.onClose}/>}

                    <div>
                        <TextField
                                className="addCommentInput"
                                id="outlined-multiline-static"
                                label="Insert Comment"
                                name="text"
                                multiline
                                rows={4}
                                value={this.state.comment.text || ''}
                                variant="outlined"
                                onChange={this.onChange}
                        />
                    </div>
                    <Button variant="outlined" color="primary" className="addCommentButton" onClick={this.handleSubmit}>Add comment</Button>
                </>
        );
    }
}

AddComment.propTypes = {
    application: PropTypes.object
};

export default AddComment;