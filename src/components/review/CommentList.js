import React, {Component} from 'react';
import '../../assets/css/application.css';
import PropTypes from 'prop-types';
import Comment from './Comment';
import AddComment from './AddComment';
import CommentService from '../../service/CommentService';

class CommentList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            comments: []
        }
        this.getComments = this.getComments.bind(this);
        this.addComments = this.addComments.bind(this);
        this.deleteComments = this.deleteComments.bind(this);
    }

    componentDidMount() {
        this.getComments(this.props.application.id)
    }

    getComments(id) {
        CommentService.getCommentsForApplication(id).then(res => {
            this.setState({
                        comments: res.data
                    }
            );
        })
    }

    addComments(comment) {
        this.setState({comments: [...this.state.comments, comment]})
    }

    deleteComments(commentId) {
        let filteredArray = this.state.comments.filter(comment => comment.id !== commentId)
        this.setState({comments: filteredArray});
    }

    render() {
        return (
                <>
                    <div style={{marginTop: '20px'}}>
                        {this.state.comments.map((comment) => (
                                <Comment onCommentDeleted={this.deleteComments} comment={comment}/>
                        ))
                        }
                    </div>
                    <AddComment onCommentAdded={this.addComments} application={this.props.application}/>
                </>
        );
    }
}

CommentList.propTypes = {
    application: PropTypes.object
};

export default CommentList;