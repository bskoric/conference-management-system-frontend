import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {Chip, Divider} from '@material-ui/core';
import '../../assets/css/comment.css';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import UserService from '../../service/UserService';
import {ROLE_USER} from '../../const/Const';
import DeleteIcon from '@material-ui/icons/Delete';
import Button from '@material-ui/core/Button';
import CommentService from '../../service/CommentService';
import AuthService from '../../service/AuthService';
import TransitionAlerts from '../layout/TransitionAlerts';

class Comment extends Component {

    emptyUser = {
        id:       0,
        name:     '',
        surname:  '',
        email:    '',
        username: '',
        title:    {
            id:   0,
            name: ''
        },
        country:  {
            id:      0,
            name:    '',
            isoCode: ''
        },
        role:     {
            id:   0,
            name: ''
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            user:          this.emptyUser,
            alertMessage:  '',
            errorSeverity: '',
            showAlert:     false
        }

        this.getUser = this.getUser.bind(this);
        this.deleteButton = this.deleteButton.bind(this);
        this.onClose = this.onClose.bind(this);
    }

    componentDidMount() {
        this.getUser(this.props.comment.user.id);
    }

    getUser(userId) {
        UserService.getUser(userId).then(res => {
            this.setState({
                        user: res.data
                    }
            );
        })
    }

    onClose() {
        this.setState({
            showAlert: false
        });
    }

    deleteButton(e) {
        e.preventDefault();

        CommentService.deleteComment(this.props.comment.id, AuthService.getLoggedInUser().id).then((response) => {
            this.setState({
                errorSeverity: 'success',
                alertMessage:  'Comment is deleted',
                showAlert:     true
            });
            this.props.onCommentDeleted(this.props.comment.id)
        }).catch((error) => {
            this.setState({
                errorSeverity: 'error',
                alertMessage:  'Error, try again',
                showAlert:     true
            });
        });
    }

    render() {
        const {comment} = this.props;
        const {user} = this.state;

        const date = new Date(comment.dateTime);
        const localDate = date.toLocaleDateString('en-US', {year: 'numeric', month: 'long', day: 'numeric'});
        const localTime = date.toLocaleTimeString('en-US', {hour12: false});

        const role = user.role.name === ROLE_USER ? 'Author' : 'Committee member';
        const avatarText = user.name.charAt(0) + user.surname.charAt(0)

        return (
                <>
                    {this.state.showAlert && <TransitionAlerts alertMessage={this.state.alertMessage}
                                                               severity={this.state.errorSeverity}
                                                               close={this.onClose}/>}

                    <div className="commentWrap">
                        <div style={{marginBottom: '10px'}}>
                            <Grid container alignItems="center">
                                <Grid item xs>
                                    <Typography variant="subtitle1">
                                        <Avatar className={'commentAvatar'}>{avatarText}</Avatar>
                                        {user.name} {user.surname} <Chip size="small" label={role}/>
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Typography gutterBottom variant="body2">
                                        <span>{localDate} </span>
                                        <span>{localTime}</span>
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Typography color="textPrimary" variant="body1" className="text">
                                {comment.text}
                            </Typography>

                            {user.id === AuthService.getLoggedInUser().id &&
                            <div style={{marginTop: '15px'}}>
                                <Button size="small" variant="outlined" onClick={this.deleteButton}>delete <DeleteIcon/></Button>
                            </div>
                            }
                        </div>
                        <Divider/>
                    </div>
                </>
        );
    }
}

Comment.propTypes = {
    comment: PropTypes.object
};

export default Comment;