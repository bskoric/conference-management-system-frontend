import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import {Chip, Divider} from '@material-ui/core';
import '../../assets/css/application.css';
import {APPLY_STATUS_DECLINED} from '../../const/Const';
import SectionService from '../../service/SectionService';
import DocumentService from '../../service/DocumentService';
import PropTypes from 'prop-types';
import FileDownload from '../document/FileDownload';
import Button from '@material-ui/core/Button';
import {withRouter} from 'react-router-dom';

class Application extends Component {

    constructor(props) {
        super(props);
        this.state = {
            section:          {},
            documentCategory: {},
            alertMessage:     '',
            errorSeverity:    '',
            showAlert:        false
        }

        this.getSection = this.getSection.bind(this);
        this.getDocumentCategory = this.getDocumentCategory.bind(this);
        this.handleButton = this.handleButton.bind(this);
    }

    componentDidMount() {
        const {document} = this.props.application;
        this.getSection(document.section.id)
        this.getDocumentCategory(document.category.id)
    }

    getSection(sectionId) {
        SectionService.getSection(sectionId).then(res => {
            this.setState({
                        section: res.data
                    }
            );
        })
    }

    getDocumentCategory(catId) {
        DocumentService.getCategory(catId).then(res => {
            this.setState({
                        documentCategory: res.data
                    }
            );
        })
    }

    handleButton(event) {
        event.preventDefault();
        this.props.history.push(
                '/conference/' + this.props.application.document.conference.id + '/apply/' + this.props.application.id + '/review')
    }

    render() {
        const {application} = this.props;
        const {document} = application;
        const textColor = application.status === APPLY_STATUS_DECLINED ? 'error' : 'textPrimary';

        return (
                <>
                    <div className="application-wrapper">
                        <div style={{marginBottom: '10px'}}>
                            <Grid container alignItems="center">
                                <Grid item xs>
                                    <Typography gutterBottom variant="h4">
                                        {document.name}
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <Typography gutterBottom variant="h6">
                                        {document.createdAt}
                                    </Typography>
                                </Grid>
                            </Grid>
                            <Typography color={textColor} variant="h6">
                                {application.status}
                            </Typography>
                            <Typography color="textPrimary" variant="body1">
                                {document.description}
                            </Typography>
                        </div>
                        <Divider/>
                        <div style={{marginTop: '10px'}}>
                            <div style={{marginTop: '10px'}}>
                                <Chip label={this.state.section.name}/>
                                <Chip color="primary" label={this.state.documentCategory.name}/>
                            </div>
                        </div>
                        <div style={{marginTop: '20px'}}>
                            <FileDownload documentId={document.id} fileName={document.fileName}/>
                        </div>
                        <div style={{marginTop: '20px'}}>
                            {this.props.reviewBtn && <Button color="primary" variant="text" onClick={this.handleButton}>Open
                                review</Button>}
                        </div>
                    </div>
                </>
        );
    }
}

Application.propTypes = {
    application: PropTypes.object
};

export default withRouter(Application);